class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }

    get id() {
        return this.productId;
    }

    set id(id) {
        this.productId = id;
    }

    get name() {
        return this.productName;
    }

    set name(name) {
        this.productName = name;
    }

    get categoryId() {
        return this.productCategoryId;
    }

    set categoryId(categoryId) {
        this.productCategoryId = categoryId;
    }

    get saleDate() {
        return this.productSaleDate;
    }

    set saleDate(saleDate) {
        this.productSaleDate = saleDate;
    }

    get quality() {
        return this.productQuality;
    }

    set quality(quality) {
        this.productQuality = quality;
    }

    get isDelete() {

    }

    set isDelete(isDelete) {
        this.productIsDelete = isDelete;
    }
}
    /**
     * create list product have 10 product
     * @returns array object product
     */
    function listProduct() 
    {
        let productArray = [];
        for(let i= 0; i< 10; i++)
        {
            let product = new Product(i, 'San pham ' + i, i+2 , (i + 2015) + "-04-" + (30-i), i + 1 , Math.floor(i % 2));
            
            productArray.push(product);
        }
        return productArray;
    }

    /**
     * Fiter product by id user ES6
     * @param array listProduct 
     * @param interger  idproduct
     * @returns array object prouct
     */
    function fiterproductbyIdEs6(listProduct, idproduct)
    {
        find = listProduct.filter(element => element.productCategoryId == idproduct);
        return find;
    }

    /**
     * Fiter product by id 
     * 
     * @param array listProduct 
     * @param interger  idproduct
     * @returns array object product
     */
    function fiterproductbyId(listProduct, idproduct)
    {
        let productArray = [];
        for (let i = 0; i < listProduct.length; i++) {
            if (listProduct[i].productCategoryId === idproduct) {
                productArray.push(listProduct[i]);
            }
        }
        return productArray;
    }

    /**
     * Fiter product with quality and isDelete user ES6
     * @param array listProduct 
     * @returns array object product
     */
    function fiterProductByQualityEs6(listProduct)
    {   
        return listProduct.filter(product => product.productQuality > 0 && product.productIsDelete == 1);
    }

    /**
     * Fiter product with quality and isDelete
     * @param array listProduct 
     * @returns array object product
     */
    function fiterProductByQuality(listProduct)
    {
        let productArray = [];
        for (let i = 0; i < listProduct.length; i++) {
            if (listProduct[i].productQuality > 0 && listProduct[i].productIsDelete == 0) {
                productArray.push(listProduct[i]);
            }
        }
        return productArray;
    }

    /**
     * Fiter product with saleDate, isDelete user ES6
     * @param array listProduct 
     * @returns array object product
     */
    function fiterProductBySaleDateEs6(listProduct)
    {
        let fiterProductbySaleDate = new Date().getTime();
        return listProduct.filter(product => new Date(product.productSaleDate).getTime() > fiterProductbySaleDate && product.productIsDelete == 0);
    }

    /**
     * Fiter product with saleDate, isDelete
     * @param array listProduct 
     * @returns array object product
     */
    function fiterProductbySaleDate(listProduct)
    {
        let timeCurrentDay = new Date().getTime();
        let productArray = [];
        for (let i = 0; i < listProduct.length; i++) {
            if (listProduct[i].productIsDelete == 0 && new Date(listProduct[i].productSaleDate).getTime() > timeCurrentDay) {
                productArray.push(listProduct[i]);
            }
        } return productArray;
    }
    
    /**
     * Get total quality product has not been deleted
     * @param array listProduct 
     * @returns interger
     */
    function totalProduct(listProduct)
    {   
        let totalQualityProduct = 0;
        listProduct.forEach(product => {
            if (product.productIsDelete = 1){
                totalQualityProduct += product.productQuality;
            }
        });
        return totalQualityProduct;
    }

    /**
     * Get total quality product has not been deleted user ES6
     * @parama array listProduct 
     * @returns interger
     */
    function totalProductEs6(listProduct)
    {
        return listProduct.reduce((totalQualityProduct, product) => {
             return totalQualityProduct + product.productQuality
        }, 0);
    }

    /**
     * Check have product by categoryId
     * @param productArray listProduct
     * @param interger categoryId 
     * @returns true false
     */
    function isHaveproductInCategoryEs6(listProduct, categoryId)
    {      
        let isProduct = listProduct.find(product => product.productCategoryId == categoryId);
        let include = listProduct.includes(isProduct);
        return include;
    }

    /**
     * Check have product by categoryId 
     * @param productArray listProduct
     * @param interger categoryId 
     * @returns  true false
     */
    function isHaveproductInCategory(listProduct, categoryId)
    {
        let productArray = [];
        for (let i = 0; i < listProduct.length; i++) {
            if (listProduct[i].productCategoryId == categoryId && listProduct[i].productIsDelete == 0) {
                return true;
            }
            return false;
        }
        return productArray;
    }

    /**
     * Fiter product by saleDate, quality use Es6
     * @param array listProduct 
     * @returns array object product
     */
    function fiterProductBySaleDateEs62(listProduct)
    {   
        let timeCurrentDay = new Date().getTime();
        return listProduct.filter(product => new Date(product.productSaleDate).getTime() > timeCurrentDay && product.productQuality > 0);
    }
    
    /**
     * Fiter product by saleDate, quality 
     * @param {*} listProduct 
     * @returns array object product
     */
    function fiterproductBySaleDate(listProduct)
    {   
        let timeCurrentDay = new Date().getTime();
        let productArray = [];
        for (let i = 0; i < listProduct.length; i++) {
            if (listProduct[i].productQuality > 0 && new Date(listProduct[i].productSaleDate).getTime() > timeCurrentDay ) {
                productArray.push(listProduct[i]);
                }
        } return productArray;
    }

// console.log(listProduct());
// console.log(fiterproductbyId(listProduct(), 3));
// console.log(fiterProductByQualityEs6(listProduct()));
// console.log(fiterProductByQuality(listProduct()));
// console.log(fiterProductbySaleDate(listProduct()));
// console.log(fiterProductBySaleDateEs6(listProduct()));
// console.log(totalProduct(listProduct()));
// console.log(totalProductEs6(listProduct()));
// console.log(isHaveproductInCategory(listProduct(), 20));
// console.log(fiterProductBySaleDateEs6(listProduct()));
// console.log(fiterproductBySaleDate(listProduct()));
// console.log(fiterproductbyIdEs6(listProduct(), 3));
// console.log(fiterProductBySaleDateEs62(listProduct(),3));

// console.log(isHaveproductInCategoryEs6(listProduct(),20));
// console.log(totalProductEs6(listProduct()));




