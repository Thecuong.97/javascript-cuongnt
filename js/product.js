class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }

    get id()
    {
        return this.id;
    }

    set id(id)
    {
        this.id = id;
    }

    get name()
    {
        return this.name;
    }

    set name(name)
    {
        this.name = name;
    }

    get categoryId()
    {
        return this.categoryId;
    }

    set categoryId(categoryId)
    {
        this.categoryId = categoryId;
    }

    get saleDate()
    {
        return this.saleDate;
    }

    set saleDate(saleDate)
    {
        this.saleDate = saleDate;
    }

    get quality()
    {
        return this.quality;
    }

    set quality(quality)
    {
        this.quality = quality;
    }

    get isDelete()
    {

    }

    get isDelete(isDelete)
    {
        this.isDelete = isDelete;
    }
}